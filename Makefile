
default:
	avr-gcc -g -Os -mmcu=atmega328p -c main.c avr_usart.c
	avr-gcc -g -mmcu=atmega328p -o main.elf main.o avr_usart.o
	avr-objcopy -j .text -j .data -O ihex main.elf main.hex

erase:
	avrdude -P /dev/ttyUSB0 -c stk500v2 -p m328p -e

flash:
	avrdude -FD -P /dev/ttyUSB0 -c stk500v2 -p m328p -U lfuse:w:0b11000111:m -U flash:w:main.hex

	#-U lfuse:w:0b11000111:m   external oscillator
	#-U lfuse:w:0b01100010:m   default

clean:
	rm -f *.out
	rm -f *.elf
	rm -f *.hex
	rm -f *.o
