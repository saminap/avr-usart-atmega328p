#include "avr_usart.h"

void USART_init(){

	UBRR0 = FOSC/16/BAUD-1;

	UCSR0B = (1<<RXEN0) | (1<<TXEN0);
	UCSR0C = (1<<USBS0) | (3<<UCSZ00);

}

unsigned char USART_receive(){

	while(!(UCSR0A & (1<<RXC0)));

	return UDR0;
}

void USART_transmit(unsigned char data){

	while( !(UCSR0A & (1<<UDRE0)));

	UDR0 = data;
}

void USART_transmit_bytes(unsigned char *word){

	while(*word){
		USART_transmit(*word);
		++word;
	}
}
