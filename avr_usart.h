#define F_CPU 8000000UL
#define FOSC F_CPU
#define BAUD 9600

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

void USART_init();
unsigned char USART_receive();
void USART_transmit(unsigned char data);
void USART_transmit_bytes(unsigned char *word);
